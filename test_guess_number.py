# -*- coding: utf-8 -*-
"""
Created on Thu Sep  9 23:16:47 2021

@author: hp
"""

import guess_number
import unittest
import unittest.mock as mock
import random

class TestGuessNumber(unittest.TestCase):
    #stub assign expected value
    # random.randint
    def test_get_random_randint_1(self):
        with mock.patch('random.randint', mock.MagicMock()) as mock_randint:
        #expect to method to return 1
            mock_randint.return_value = 1
            result = guess_number.guess_int(1,10)
            self.assertEqual(result, 1)
            mock_randint.assert_called_once()

    @mock.patch('random.randint', return_value=2)
    def test_get_random_randint_2(self,mock_randint):
        #expect to method to return 2
        mock_randint.guess_int.return_value = 2
        result = guess_number.guess_int(1,10)
        self.assertEqual(result, 2)
        mock_randint.assert_called_once()
        
    #stub assign expected value
    # random.randint
    def test_get_random_uniform_5(self):
        with mock.patch('random.uniform', mock.MagicMock()) as mock_uniform:
        #expect to method to return 5
            mock_uniform.return_value = 5
            result = guess_number.guess_float(1,10)
            self.assertEqual(result, 5)
            mock_uniform.assert_called_once()
            
    @mock.patch('random.uniform', return_value=6)
    def test_get_random_uniform_6(self,mock_uniform):
        #expect to method to return 6
        mock_uniform.guess_float.return_value = 6
        result = guess_number.guess_float(1,10)
        self.assertEqual(result, 6)
        mock_uniform.assert_called_once()